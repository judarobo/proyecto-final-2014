#include <iostream>
#include <cstdlib>
#include <cmath>

#define TAB  "\t"

using namespace std;

const int N_intervals = 700;

double Rand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

// logistic map
inline double Logistic_map(double r,double xn){
	return r*xn*(1-xn);
}

inline double Derivative_Logistic(double r,double xn){
	return r -2*r*xn;
}

// tent map
inline double Tent_map(double r,double xn){
	double z;
	if(xn < 0.5)
		z = r*xn;
	else
		z = r - r*xn;
	return z;
}

inline double Derivative_Tent(double r,double xn){
	double z;
	if(xn < 0.5)
		z = r;
	else
		z = -r;
	return z;
}

double bif_diagram(double f(double,double),double x0,int n,int N,double r){
	double xn = 0.0;
	for (int i = 0; i < N; ++i){
		xn = f(r,x0);
		x0 = xn;
		if(i >= n)
			cout << r << TAB << xn << endl;
	}
}

double Lyapunov_exponent(double f(double,double),double der(double,double),double x0,int n,int N,double r){
	double xn = 0;
	for (int i = 0; i < n; ++i)
	{
		xn = f(r,x0);
		x0 = xn;
	}

	double *Z;
	Z = new double[N];

	for (int j = 0; j < N; ++j)
	{
		xn = f(r,x0);
		Z[j] = log(fabs(der(r,xn)));
		x0 = xn;
	}

	double s = Z[0];
	for (int i = 1; i < N; ++i)
	{
		s += Z[i];
	}
	double L = s/N;
	return L;
}

int main(int argc, char const *argv[])
{
	double r_min = 2.9;
	//double r_min = 0.1;
	double r_max = 4.;
	//double r_max = 2.;
	double x0,r = r_min;
	
	double L;
	for (int i = 0; i < N_intervals; ++i){
		r += (r_max - r_min)/N_intervals;
		x0 = Rand(0,1);
		bif_diagram(Tent_map,x0,1000,1500,r);
		L = Lyapunov_exponent(Logistic_map,Derivative_Logistic,x0,300,10000,r);
		cout << r << TAB << L  << endl;
	}

	return 0;
}
